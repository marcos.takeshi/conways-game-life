# Conway's Game Of Life

## Project Description

Simulation of cell life with python

Rules:

- A live cell with fewer than two live neighbors dies (underpopulation).
- A live cell with two or three live neighbors remains alive (survival).
- A live cell with more than three live neighbors dies (overpopulation).
- A dead cell with exactly three live neighbors becomes alive (reproduction).

## Usage

This is a python program so it's executed with the following command:

```
python3 gamelife.py
```

![GAMELIFE](images/gamelife.png "Game Of Life")


